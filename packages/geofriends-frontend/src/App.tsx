import React, { useEffect, useState } from "react";
import logo from "./logo.svg";
import "./App.css";
import { config } from "./Config";

function App() {
  const [text, setText] = useState("Loading...");

  const getBackend = () => {
    fetch(config.backendUrl)
      .then((res) => {
        return res.text();
      })
      .then((text) => setText(text))
      .catch((err) => {
        console.log(err);
        setText("Error");
      });
  };

  useEffect(() => {
    getBackend();
    const timer = setInterval(getBackend, 1000);
    return () => clearInterval(timer);
  });

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>{text}</p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
