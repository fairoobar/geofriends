import { PrismaClient } from "@prisma/client";
import { Express, Response } from "express";

export const addHealthcheck = (client: PrismaClient, app: Express) => {
  app.get("/healthz", (req: any, res: Response) => {
    client
      .$connect()
      .then(() => {
        res.sendStatus(200);
      })
      .catch((e: Error) => {
        console.log(`Healthcheck failed: ${e.message}`);
        res.sendStatus(500);
      })
      .finally(() => {
        client.$disconnect();
      });
  });
};
