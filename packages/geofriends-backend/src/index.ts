import express, { Response, Express } from "express";
import cors from "cors";
import { PrismaClient } from "@prisma/client";
import { config } from "dotenv";
import { addHealthcheck } from "./handlers/healthcheck.js";

if (process.env.NODE_ENV === "test") {
  console.log("Loading test config");
  config();
} else {
  console.log("Loading environment variables");
}

const app: Express = express();
app.use(cors());
const port = 3001;

const prisma: PrismaClient = new PrismaClient();

addHealthcheck(prisma, app);

app.get("/", (req: any, res: { send: (arg0: string) => void }) => {
  console.log("recieved request");
  res.send("GeoFriends!");
});

app.listen(port, () => {
  console.log(`GeoFriends on port ${port}`);
});
