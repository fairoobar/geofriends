-- CreateTable
CREATE TABLE "Game" (
    "id" SERIAL NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "name" TEXT NOT NULL,
    "firstNodeId" INTEGER,

    CONSTRAINT "Game_pkey" PRIMARY KEY ("id")
);
